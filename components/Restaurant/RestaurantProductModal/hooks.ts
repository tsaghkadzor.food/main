import { createSharedComposable } from "@vueuse/core"
import type { MenuProduct } from "~/composable/restaurant/useRestaurantMenu"

export const useRestaurantProductModal = createSharedComposable(() => {
  const product = ref<MenuProduct>()
  const isOpen = ref(false)

  const close = () => {
    isOpen.value = false
  }
  const open = (currentProduct: MenuProduct) => {
    product.value = currentProduct
    isOpen.value = true
  }

  return {
    isOpen: readonly(isOpen),
    product: readonly(product),
    close,
    open,
  }
})
