export type MultilangString = {
  en: string,
  ru: string,
  am: string,
}