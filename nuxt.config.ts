import postcssOptions from './postcss.config'
import { nuxtConfigAlias } from './src/nuxt/config/nuxtConfigAlias'
import { nuxtConfigGlobal } from './src/nuxt/config/nuxtConfigGlobal'
import { nuxtConfigPwa } from './src/nuxt/config/nuxtConfigPwa'

const SUPABASE_BACKET_DOMAIN = 'fdfbazeestxdjmmwlskv.supabase.co'
const globals = nuxtConfigGlobal()

// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
  globals,
  // dir: nuxtConfigDir(),
  // alias: nuxtConfigAlias(),
  components: {
    global: true,
    dirs: ['~/components'],
  },
  modules: [
    'nuxt-typed-router',
    '@nuxt/image-edge',
    '@nuxtjs/supabase',
    '@kevinmarrec/nuxt-pwa',
  ],
  image: {
    domains: [SUPABASE_BACKET_DOMAIN],
    staticFilename: '[publicPath]/images/[name]-[hash][ext]',
    alias: {
      backetRestaurant: `https://${SUPABASE_BACKET_DOMAIN}/storage/v1/object/public/cahkadzor/`,
    },
  },
  plugins: [
    { src: '~/plugins/i18n.ts', mode: 'all' },
  ],
  css: [
    '~/assets/css/main.css',
  ],
  build: {
    transpile: ['@heroicons/vue'],
    postcss: {
      postcssOptions,
    },
  },
  pwa: nuxtConfigPwa(),
})
