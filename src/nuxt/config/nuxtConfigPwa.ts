import path from 'path'
import { ProjectType, PROJECT_TYPE } from '../env/projectType'

const APP_DISPLAY_NAME = 'Tsaghkadzor.Food'
const APP_DISPLAY_SHORT_NAME = 'Tsaghkadzor'

const ENABLED_MOBILE_APP = true //PROJECT_TYPE === ProjectType.MAIN

export const nuxtConfigPwa = () => {
  return {
    icon: {
      source: path.join(__dirname, '../../../public/pwa/icon.png'),
      fileName: 'icon.png',
      sizes: [64, 120, 144, 152, 192, 384, 512],
      // targetDir
    },
    workbox: {
      enabled: true
    },
    manifest: {
      name: APP_DISPLAY_NAME,
      short_name: APP_DISPLAY_SHORT_NAME,
      //description: process.env.npm_package_description!,
      lang: 'en',
      display: 'standalone',
      background_color: '#ffffff',
      theme_color: '#000000',
      icons: [],
    },
    meta: {
      dir: "ltr",
      lang: "en",
      name: APP_DISPLAY_NAME,
      short_name: APP_DISPLAY_SHORT_NAME,
      // author: process.env.npm_package_author_name!,
      // description: process.env.npm_package_description!,
      favicon: true,
      mobileApp: ENABLED_MOBILE_APP,
      mobileAppIOS: ENABLED_MOBILE_APP,
      appleStatusBarStyle: false,
      theme_color: undefined,
      ogType: 'website',
      ogSiteName: true,
      ogTitle: true,
      ogDescription: true,
      ogImage: true,
      ogHost: undefined,
      ogUrl: true,
      twitterCard: undefined,
      twitterSite: undefined,
      twitterCreator: undefined,
    },
  }
}