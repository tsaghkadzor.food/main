import type { NuxtConfig } from 'nuxt/config'

/**
 * https://nuxtjs.org/docs/configuration-glossary/configuration-global-name/#the-globals-property
 */
export const nuxtConfigGlobal = ():NuxtConfig['globals'] => ({
  id: () => '__tsaghkadzor_food',
  nuxt: () => '$tsaghkadzorFood',
  context: () => '__TSAGHKADZOR_FOOD__',
  pluginPrefix: () => 'tsaghkadzor_food',
  readyCallback: () => 'onTsaghkadzorFoodReady',
  loadedCallback: () => '_onTsaghkadzorFoodLoaded',
})
