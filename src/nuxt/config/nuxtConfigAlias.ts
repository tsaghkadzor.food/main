import type { NuxtConfig } from 'nuxt/config'

export const nuxtConfigAlias = ():NuxtConfig['alias'] => {
  return {
    '@': 'src'
  }
}