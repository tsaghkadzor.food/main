import type { NuxtConfig } from 'nuxt/config'
import path from 'path'
import { ProjectType, PROJECT_TYPE } from '../env/projectType'

export const projectsDirs = <const>{
  [ProjectType.MAIN]: path.join(__dirname, '../../../main'),
  [ProjectType.ADMIN]: path.join(__dirname, '../../../admin'),
}

export const projectDir = projectsDirs[PROJECT_TYPE]

const assetsDir = `${projectDir}/assets`
const layoutsDir = `${projectDir}/layouts`
const appDir = `${projectDir}/app`
const middlewareDir = `${projectDir}/middleware`
const pagesDir = `${projectDir}/pages`
const publicDir = `${projectDir}/public`
const staticDir = `${projectDir}/public`
const storeDir = `${projectDir}/store`

export const nuxtConfigDir = ():NuxtConfig['dir'] => {
  return {
    assets: assetsDir,
    layouts: layoutsDir,
    app: appDir,
    middleware: middlewareDir,
    pages: pagesDir,
    public: publicDir,
    static: staticDir,
    store: storeDir,
  }
}
