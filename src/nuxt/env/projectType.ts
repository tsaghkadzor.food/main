export enum ProjectType {
  MAIN = 'MAIN',
  ADMIN = 'ADMIN',
}

export const PROJECT_TYPE = <ProjectType>process.env.PROJECT_TYPE
export const AVALIBLE_PROJECT_TYPES:Array<ProjectType> = [ProjectType.MAIN, ProjectType.ADMIN]

if (!AVALIBLE_PROJECT_TYPES.includes(PROJECT_TYPE)) {
  throw new Error(`\n-------------\nError projectType: unknown projectType; \nprocess.env.PROJECT_TYPE=${PROJECT_TYPE}\n-------------\n`)
}

console.log(`> INIT ${PROJECT_TYPE} project`)
