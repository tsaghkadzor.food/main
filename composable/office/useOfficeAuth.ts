import type { Ref } from 'nuxt/dist/app/compat/capi'
import { useSupabaseClient } from '@nuxtjs/supabase/dist/runtime/composables/useSupabaseClient'
import { createSharedComposable } from '@vueuse/core'

export const useOfficeAuth = createSharedComposable(() => {
  const client = useSupabaseClient()

  const signIn = (phone: Ref<string>, password: Ref<string>) => {
    client.auth.signIn({
      phone: phone.value,
      password: password.value,
    })
  }

  return {
    signIn,
  }
})
