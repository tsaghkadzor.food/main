import { createSharedComposable } from '@vueuse/core'

export const useDeliveryAddress = createSharedComposable(() => {
  const selectedDeliveryAddressId = useState<undefined|number>('SelectedDeliveryAddressesId', () => undefined)
  const allDeliveryAddresses = useState('AllDeliveryAddresses', () => {
    return [
      {
        title: 'Aurora Resort Tsakhkadzor',
        id: 1,
      },
      {
        title: 'Multi Rest House',
        id: 2,
      },
      {
        title: 'Tsaghkadzor Marriott Hotel',
        id: 3,
      },
    ]
  })

  const selectedAddress = computed(() => {
    const selectedDeliveryAddressIdVal = selectedDeliveryAddressId.value

    if (typeof selectedDeliveryAddressIdVal !== 'number') {
      return undefined
    }

    return allDeliveryAddresses.value.find(({id}) => id === selectedDeliveryAddressIdVal)
  })

  const setDeliveryAddress = (addressId: number) => {
    if (allDeliveryAddresses.value.findIndex(({id}) => id === addressId) > -1) {
      selectedDeliveryAddressId.value = addressId
    }
  }

  return {
    allDeliveryAddresses,
    selectedAddress,
    setDeliveryAddress,
  }
})
