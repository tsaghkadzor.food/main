import { BeforeInstallPromptEvent, Options } from './types';

function getPWADisplayMode() {
  const isStandalone = window.matchMedia('(display-mode: standalone)').matches;
  if (document.referrer.startsWith('android-app://')) {
    return 'twa';
    // @ts-ignore
  } else if (navigator.standalone || isStandalone) {
    return 'standalone';
  }
  return 'browser';
}

export const usePwaInstall = (options?: Options) => {
  const isBrowser = getPWADisplayMode() === 'browser'

  if (!isBrowser) {
    return {
      isBrowser: false,
      isInstalled: ref(true),
      canInstall: ref(false),
      install: () => Promise.resolve()
    }
  } else if (!process.client) {
    return {
      isBrowser: true,
      isInstalled: ref(false),
      canInstall: ref(false),
      install: () => Promise.resolve()
    }
  }

  let beforeInstallPromptEvent: BeforeInstallPromptEvent|undefined
  const isInstalled = ref(false)
  const canInstall = ref(false)

  const install = async () => {
    if (!beforeInstallPromptEvent) return;

    beforeInstallPromptEvent.prompt();

    const { outcome } = await beforeInstallPromptEvent.userChoice;

    if (outcome === 'accepted') {
      isInstalled.value = true;
    }
  }

  window.addEventListener('beforeinstallprompt', (e: BeforeInstallPromptEvent) => {
    e.preventDefault()
    isInstalled.value = false
    canInstall.value = true
    beforeInstallPromptEvent = e
  })

  watchEffect((onCleanup) => {
    const handler = () => {
      isInstalled.value = true;
      // For apply setIsInstalled(true) when install and popup new pwa
      window.location.reload();
    };

    window.addEventListener('appinstalled', handler);

    onCleanup(() => {
      window.removeEventListener('appinstalled', handler);
    })
  })

  return { isInstalled, canInstall, install, isBrowser };
}
