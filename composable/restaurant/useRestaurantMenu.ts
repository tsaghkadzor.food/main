import type { Ref } from 'nuxt/dist/app/compat/capi'
import { createSharedComposable } from '@vueuse/core'
import { MultilangString } from '~/components/App/AppMultilangString/types'
import { useRestaurantMenuGroups } from './useRestaurantMenuGroups'

export type MenuProduct = {
  id: number,
  available: boolean,
  restaurantId: number,
  price: {
    amd: number
  },
  card: {
    title: MultilangString,
    cover: {
      src: string,
    }
  },
  productGroupId: number,
}

export type MenuProductsGroup = {
  id: number,
  title: MultilangString,
  products: MenuProduct[],
  page: {
    hash: string,
  },
}

const useGroupProducts = (products:Ref<MenuProduct[]>) => {
  const {menuGroupsById} = useRestaurantMenuGroups()
  return computed<MenuProductsGroup[]>(() => products.value.reduce((productGroups, product) => {
    let findedGroup:MenuProductsGroup|undefined = productGroups.find(({id}) => id === product.productGroupId)

    if (!findedGroup) {
      const currentGroup = menuGroupsById.value.get(product.productGroupId)
      findedGroup = {
        id: currentGroup.id,
        title: currentGroup.title,
        products: [product],
        page: {
          hash: `group-${currentGroup.id}`,
        },
      }

      productGroups.push(findedGroup)
    } else {
      findedGroup.products.push(product)
    }

    return productGroups
  }, <MenuProductsGroup[]>[]))
}

export const useRestaurantMenu = createSharedComposable(() => {
  const rawProducts = ref([])
  const products = computed(() => {
    return rawProducts.value.map<MenuProduct>((product) => {
      return {
        id: product.id,
        productGroupId: product.product_group,
        restaurantId: product.restaurant_id,
        available: product.available,
        price: {
          amd: product.price,
        },
        card: {
          title: {
            en: product.title_en,
            am: product.title_am,
            ru: product.title_ru,
          },
          cover: {
            src: '/backetRestaurant/restaurant-card-1.jpg',
          },
        },
      }
    })
  })
  
  const groupedProducts = useGroupProducts(products)
  const setMenuRawData = (data:any[]) => {
    if (Array.isArray(data)) {
      rawProducts.value = data
    }
  }

  return {
    products: products,
    groupedProducts,
    setMenuRawData,
  }
})
