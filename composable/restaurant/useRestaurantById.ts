import { Ref } from 'nuxt/dist/app/compat/capi'
import { useRestaurantsStore } from './useRestaurantsStore'

export const useRestaurantById = (restaurantIdRef:Ref<number>) => {
  const {restaurants} = useRestaurantsStore()

  const restaurant = computed(() => {
    const restaurantId = restaurantIdRef.value
    
    return restaurants.value.find(({id}) => id === restaurantId)
  })

  return {
    restaurant: readonly(restaurant),
  }
}
