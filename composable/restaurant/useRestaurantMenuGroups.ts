import type { MultilangString } from "~/components/App/AppMultilangString/types"
import { createSharedComposable } from "@vueuse/core"

type MenuGroup = {
  id: number,
  title: MultilangString,
}

export const useRestaurantMenuGroups = createSharedComposable(() => {
  const menuGroupsRawData = ref<any[]>([])
  const menuGroups = computed<MenuGroup[]>(() => {
    return menuGroupsRawData.value.map<MenuGroup>((rawGroup) => ({
        id: rawGroup.id,
        title: {
          en: rawGroup.title_en,
          ru: rawGroup.title_ru,
          am: rawGroup.title_am,
        }
      }))
  })
  const menuGroupsById = computed(() => {
    return new Map(menuGroups.value.map((group) => [group.id, group]))
  })

  const setMenuGroupRawData = (data:any[]) => {
    menuGroupsRawData.value = data
  }

  return {
    menuGroupsById,
    menuGroups: readonly(menuGroups),
    setMenuGroupRawData,
  }
})
