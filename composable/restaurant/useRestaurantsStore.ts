import { createSharedComposable } from '@vueuse/core'
import type { MultilangString } from '~/components/App/AppMultilangString/types'

export type Restaurant = {
  id: number,
  page: {
    title: MultilangString,
    link: string,
  },
  card: {
    title: MultilangString,
    cover: {
      src: string,
    },
  },
  minOrderPrice: {
    amd: number,
  },
}

export const useRestaurantsStore = createSharedComposable(() => {
  const rawRestaurants = useState('rawRestaurants', () => [])
  const restaurants = computed(() => {
    return rawRestaurants.value.map((dbData) => {
      const {id} = dbData
  
      return {
        id,
        page: {
          title: {
            en: dbData.title_en,
            ru: dbData.title_ru,
            am: dbData.title_am,
          },
          link: `/restaurant/${id}/`,
        },
        card: {
          title: {
            en: dbData.title_en,
            ru: dbData.title_ru,
            am: dbData.title_am,
          },
          cover: {
            src: `/backetRestaurant/restaurant-card-${id}.jpg`,
          }
        },
        minOrderPrice: {
          amd: dbData.minOrderPrice,
        }
      }
    })
  })

  const setRawRestaurants = (data:any[]) => {
    rawRestaurants.value = Array.isArray(data) ? data : []
  }

  return {
    setRawRestaurants,
    restaurants: readonly(restaurants),
  }
})
