import { useRestaurantsStore } from '~/composable/restaurant/useRestaurantsStore'
import { useUserAuth } from '~/composable/user/useUserAuth'

/**
 * Должно быть вызвано только один раз!
 */
export const useLoadRestaurants = async () => {
  const {client} = useUserAuth()
  
  const {setRawRestaurants} = useRestaurantsStore()

  const {data: rawRestaurants} = await useAsyncData(
    'rawAllRestaurants',
    async () => {
      const dbResponce = await client.from('restaurant')
      const {data} = dbResponce

      return data
    }, {
      default: () => [],
    }
  )

  watch(rawRestaurants, (data) => setRawRestaurants(data), {immediate: true})
}