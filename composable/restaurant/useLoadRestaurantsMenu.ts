import { Ref } from 'nuxt/dist/app/compat/capi';
import { useUserAuth } from '~/composable/user/useUserAuth';
import { useRestaurantMenu } from './useRestaurantMenu';

/**
 * Должно быть вызвано только один раз!
 */
export const useLoadRestaurantsMenu = async (restaurantId: Ref<number>) => {
  const {client} = useUserAuth()

  const {data: menuRawData} = await useAsyncData(
    'restaurant_menu',
    async () => {
      const dbResponse = await client.from('product').select(`
        id,
        available,
        title_ru,
        title_en,
        title_am,
        price,
        product_group
      `).eq('restaurant_id', restaurantId.value)

      return dbResponse.data
    },
    {
      default: () => [],
      watch: [restaurantId],
      immediate: true,
    }
  )

  const { setMenuRawData } = useRestaurantMenu()
  watch(menuRawData, (data) => setMenuRawData(data), {immediate: true})
}
