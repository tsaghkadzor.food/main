import { useRestaurantById } from './useRestaurantById'
import { useRestaurantIdByPage } from './useRestaurantIdByPage'

export const useRestaurantByPage = () => {
  const {restaurantId} = useRestaurantIdByPage()
  const {restaurant} = useRestaurantById(restaurantId)
  
  return {
    restaurantId,
    restaurant,
  }
}
