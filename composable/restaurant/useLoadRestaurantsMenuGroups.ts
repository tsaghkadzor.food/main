import { useRestaurantMenuGroups } from '~/composable/restaurant/useRestaurantMenuGroups';
import { useUserAuth } from '~/composable/user/useUserAuth';

/**
 * Должно быть вызвано только один раз!
 */
export const useLoadRestaurantsMenuGroups = async () => {
  const {client} = useUserAuth()

  const {data: menuGroupsRawData} = await useAsyncData(
    'menu_groups',
    async () => {
      const dbResponse = await client.from('product_group').select(`
        id,
        title_ru,
        title_en,
        title_am
      `)

      return dbResponse.data
    },
    {
      default: () => [],
    }
  )

  const { setMenuGroupRawData } = useRestaurantMenuGroups()
  watch(menuGroupsRawData, (data) => setMenuGroupRawData(data), {immediate: true})
}
