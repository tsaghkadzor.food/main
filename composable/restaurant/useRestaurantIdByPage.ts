import { createSharedComposable } from "@vueuse/core"

export const useRestaurantIdByPage = createSharedComposable(() => {
  const route = useRoute()
  const restaurantId = computed(() => {
    const id = route.params.restaurantId
    
    if (typeof id !== 'string') {
      throw new Error()
    }
    
    return Number(id)
  })

  return {restaurantId}
})
