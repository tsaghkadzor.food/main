import type { Ref } from 'nuxt/dist/app/compat/capi'
import { createSharedComposable } from '@vueuse/core'
import type { useSupabaseClient } from '@nuxtjs/supabase/dist/runtime/composables/useSupabaseClient'
import type { useSupabaseUser } from '@nuxtjs/supabase/dist/runtime/composables/useSupabaseUser'

export const useUserAuth = createSharedComposable(() => {
  const client = (useSupabaseClient as any as typeof useSupabaseClient)()
  const user = (useSupabaseUser as any as typeof useSupabaseUser)()

  const signIn = async (phone: Ref<string>, password: Ref<string>) => {
    await client.auth.signIn({
      email: phone.value,
      password: password.value,
    })
  }

  const signUp = async (phone: Ref<string>, password: Ref<string>) => {
    await client.auth.signUp({
      email: phone.value,
      password: password.value,
    }, {
      redirectTo: '/',
    })
  }

  const signOut = async () => {
    await client.auth.signOut()
  }

  return {
    user,
    client,
    signIn,
    signUp,
    signOut,
  }
})
