import type { User, SupabaseClient } from '@supabase/supabase-js'
import { MenuProduct } from './restaurant/useRestaurantMenu'
import { createSharedComposable } from '@vueuse/core'
import { useUserAuth } from './user/useUserAuth'

export type BasketItem = {
  product: MenuProduct
  count: number
  itemId: string
}

export type BasketSlimItem = {
  productId: number
  count: number
  itemId: string
}

const basketToSlimData = (items: BasketItem[]): BasketSlimItem[] => {
  return items.map(item => {
    return {
      itemId: item.itemId,
      count: item.count,
      productId: item.product.id
    }
  })
}

const slimDataToBasket = (items: BasketSlimItem[], products): BasketItem[] => {
  console.log('productsproducts', products)
  return items.map(item => {
    return {
      itemId: item.itemId,
      count: item.count,
      product: products.find(({id}) => id === item.productId),
    }
  })
}

const saveBasketToBd = async (
  client: SupabaseClient,
  user: User,
  basket: BasketItem[],
) => {
  await client
    .from('basket').upsert(
      {
        items: JSON.stringify(basketToSlimData(basket)),
        user_id: user.id,
      },
    )
    .eq('user_id', user.id)
    .throwOnError(true)
}

export const useBasket = createSharedComposable(() => {
  const {user, client} = useUserAuth()
  const basket = ref<BasketItem[]>([])
  const currentRestaurantId = ref<number>()

  const addToBasket = async (product: MenuProduct, orderOptions = {}) => {
    if (!user.value) {
      console.debug('[useBasket.addToBasket]: user not auth')
      return
    }

    basket.value.push({
      count: 1,
      product,
      itemId: product.id.toString()
    })

    // await saveBasketToBd(client, user.value, basket.value)
  }

  const removeItem = async (removingItemId: string) => {
    const newBasket = basket.value.filter(({itemId}) => itemId !== removingItemId)
    
    // await saveBasketToBd(client, user.value, newBasket)

    basket.value = newBasket
  }

  const clearBasket = async () => {
    // await saveBasketToBd(client, user.value, [])

    basket.value = []
  }

  const totalPrice = computed(() => {
    let sum = basket.value.reduce((total, {count, product: {price}}) => {
      return total + count * price.amd
    }, 0)

    sum += 500

    return sum
  })

  const checkHasProduct = computed(() => (productId:number) => {
    return basket.value.findIndex(({product: {id}}) => id === productId) > -1
  })

  const productsCount = computed(() => basket.value.reduce((total, {count}) => total + count, 0))

  const isOpenBasket = ref(false)
  const openBasket = () => isOpenBasket.value = true
  const closeBasket = () => isOpenBasket.value = false

  return {
    restaurantId: readonly(currentRestaurantId),
    basket: readonly(basket),
    totalPrice: readonly(totalPrice),
    checkHasProduct,
    isOpenBasket: readonly(isOpenBasket),
    productsCount: readonly(productsCount),
    addToBasket,
    removeItem,
    clearBasket,
    openBasket,
    closeBasket,
  }
})
