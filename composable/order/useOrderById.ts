import { Ref } from 'nuxt/dist/app/compat/capi'
import { useOrdersStore } from './useOrdersStore'

export const useOrderById = (restaurantIdRef:Ref<number>) => {
  const {orders} = useOrdersStore()

  return computed(() => {
    const restaurantId = restaurantIdRef.value
    
    return orders.value.find(({id}) => id === restaurantId)
  })
}
