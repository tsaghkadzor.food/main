import { createSharedComposable } from '@vueuse/core'
import { BasketItem } from '../useBasket'

export enum OrderStatus {
  created = 'created',
  confirmed = 'confirmed',
}

export type Order = {
  id: number
  basket: Readonly<BasketItem[]>
  status: OrderStatus
  page: {
    link: string
  }
}

export const useOrdersStore = createSharedComposable(() => {
  const orders = ref<Order[]>([])

  const addOrder = (order: Order) => {
    orders.value.push(order)
  }

  return {
    addOrder,
    orders,
  }
})
