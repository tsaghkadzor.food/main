import type { User, SupabaseClient } from '@supabase/supabase-js'
import type { Order } from './useOrdersStore'
import { useOrdersStore, OrderStatus } from './useOrdersStore'
import { useBasket } from '../useBasket'
import { useUserAuth } from '../user/useUserAuth'

const saveOrderToDb = async (
  client: SupabaseClient,
  user: User,
  order: Omit<Order, 'id'>
) => {
  await client.from('order').insert({

  })
}

export const useCreateOrder = () => {
  const { addOrder } = useOrdersStore()
  const {user, client} = useUserAuth()
  const { basket, clearBasket } = useBasket()

  const createOrder = async () => {
    const id =  Math.ceil(Math.random() * 10e8)
    const order: Order = {
      id,
      basket: basket.value,
      status: OrderStatus.created,
      page: {
        link: `/order/${id}`
      }
    }

    await saveOrderToDb(client, user.value, order)

    addOrder(order)
    clearBasket()

    return order
  }

  return {
    createOrder,
  }
}
